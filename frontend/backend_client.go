package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// URLs for sending requests to backend
const (
	BaseBackendURL    = "http://backend:8000/api"
	GoogleAuthURL     = BaseBackendURL + "/users/authenticate/google"
	AuthURL           = BaseBackendURL + "/users/authenticate"
	LogoutURL         = BaseBackendURL + "/users/logout"
	ForgotPasswordURL = BaseBackendURL + "/users/forgot"
	ResetPasswordURL  = BaseBackendURL + "/users/reset"
	BaseUserURL       = BaseBackendURL + "/users"
)

// AuthRequest contains data for authorizing user logged in via email and password
type AuthRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// OAuthRequest contains data for authorizing user logged in via google API
type OAuthRequest struct {
	Token string `json:"token"`
}

// OAuthResponse contains data for authorization from backed of user
type OAuthResponse struct {
	ID    int64  `json:"id"`
	Token string `json:"token"`
}

// ForgotPasswordRequest contains data for sending forgot password request
type ForgotPasswordRequest struct {
	Email string `json:"email"`
}

// ForgotPasswordResponse contains response data for forgot password
type ForgotPasswordResponse struct {
	Token string `json:"token"`
}

// ResetPasswordRequest contains data for resetting password of user
type ResetPasswordRequest struct {
	Password string `json:"password"`
	Token    string `json:"token"`
}

// User defines the structure to be used in users REST API
type User struct {
	ID        int64  `json:"id"`
	Email     string `json:"email"`
	Password  string `json:"password,omitempty"`
	Fullname  string `json:"fullname"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
	CreatedAt int64  `json:"createdAt"`
	UpdatedAt int64  `json:"updatedAt"`
	Version   int    `json:"version"`
}

// BackendClient is http client to send requests to backend
type BackendClient struct {
	client *http.Client
}

// NewBackendClient creates new backend client
func NewBackendClient() *BackendClient {
	return &BackendClient{
		client: &http.Client{},
	}
}

// DeleteUser sends delete request to backend to delete user data
func (bc *BackendClient) DeleteUser(req UserCookies) error {
	url := BaseUserURL + "/" + req.UserID
	delReq, err := http.NewRequest(http.MethodDelete, url, nil)
	if err != nil {
		log.Println("Failed making new request", err.Error())
		return err
	}

	// Set request headers
	delReq.Header.Set("token", req.Token)

	resp, err := bc.client.Do(delReq)
	if err != nil {
		log.Println("Failed sending request", err.Error())
		return nil
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Post request failed, status code %v", resp.StatusCode)
	}

	return nil
}

// CreateUser sends put request to backend to update user data, returns the created user data
func (bc *BackendClient) CreateUser(data *User) (*User, error) {
	// marshal User to json
	jsonData, err := json.Marshal(data)
	if err != nil {
		log.Println("Marshal failed", err.Error())
		return nil, err
	}

	resp, err := http.Post(BaseUserURL, "application/json", bytes.NewBuffer(jsonData))
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Post request failed, status code %v", resp.StatusCode)
	}

	defer resp.Body.Close()
	var respData User

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// Read response data
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return nil, err
	}

	return &respData, nil
}

// UpdateUser sends put request to backend to update user data
func (bc *BackendClient) UpdateUser(req UserCookies, data *User) error {
	url := BaseUserURL + "/" + req.UserID

	// marshal User to json
	json, err := json.Marshal(data)
	if err != nil {
		log.Println("Marshal failed", err.Error())
		return err
	}

	putReq, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(json))
	if err != nil {
		log.Println("Failed making new request", err.Error())
		return err
	}

	// Set request headers
	putReq.Header.Set("Content-Type", "application/json")
	putReq.Header.Set("token", req.Token)

	resp, err := bc.client.Do(putReq)
	if err != nil {
		log.Println("Failed sending request", err.Error())
		return nil
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Post request failed, status code %v", resp.StatusCode)
	}

	return nil
}

// GetUser sends get request to backend to get user data
func (bc *BackendClient) GetUser(req UserCookies) (*User, error) {
	url := BaseUserURL + "/" + req.UserID

	getReq, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Println("Failed making new request", err.Error())
		return nil, err
	}

	getReq.Header.Set("token", req.Token)

	resp, err := bc.client.Do(getReq)
	if err != nil {
		log.Println("Failed to get user data", err)
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Post request failed, status code %v", resp.StatusCode)
	}

	defer resp.Body.Close()
	var respData User

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("ReadAll failed", err.Error())
		return nil, err
	}

	// Read response data
	err = json.Unmarshal(body, &respData)
	if err != nil {
		log.Println("Unmarshal failed", err.Error())
		return nil, err
	}

	return &respData, nil
}

// PostGoogleAuth sends post request to authenticate user via google token
func (bc *BackendClient) PostGoogleAuth(req OAuthRequest) (*OAuthResponse, error) {
	// Prepare data for backend authentication
	data, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	return bc.postAuthRequest(GoogleAuthURL, data)
}

// PostAuth sends post request to authenticate user via email and password
func (bc *BackendClient) PostAuth(req AuthRequest) (*OAuthResponse, error) {
	// Prepare data for backend authentication
	data, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	return bc.postAuthRequest(AuthURL, data)
}

// Logout sends post request to logout user
func (bc *BackendClient) Logout(req OAuthResponse) error {
	// Prepare data for backend logout
	data, err := json.Marshal(req)
	if err != nil {
		return err
	}

	postReq, err := http.NewRequest(http.MethodPost, LogoutURL, bytes.NewBuffer(data))
	if err != nil {
		log.Println("Failed making new request", err.Error())
		return err
	}

	postReq.Header.Set("token", req.Token)

	// Send post request
	resp, err := bc.client.Do(postReq)
	if err != nil {
		log.Println("Failed to get user data", err)
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Post request failed, status code %v", resp.StatusCode)
	}

	return nil
}

// SendForgotPasswordRequest send post request to backend for forgot password, returns token to reset the password
func (bc *BackendClient) SendForgotPasswordRequest(req ForgotPasswordRequest) (*ForgotPasswordResponse, error) {
	// Prepare data for forgot password request
	data, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	resp, err := http.Post(ForgotPasswordURL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Post request failed, status code %v", resp.StatusCode)
	}

	defer resp.Body.Close()
	var respData ForgotPasswordResponse

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// Read response data
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return nil, err
	}

	return &respData, nil
}

// SendResetPasswordRequest send post request to backend to reset user password
func (bc *BackendClient) SendResetPasswordRequest(req ResetPasswordRequest) error {
	// Prepare data for reset password request
	data, err := json.Marshal(req)
	if err != nil {
		return err
	}

	resp, err := http.Post(ResetPasswordURL, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("Post request failed, status code %v", resp.StatusCode)
	}

	return nil
}

func (bc *BackendClient) postAuthRequest(url string, data []byte) (*OAuthResponse, error) {
	// Send post request
	resp, err := http.Post(url, "application/json", bytes.NewBuffer(data))
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("Post request failed, status code %v", resp.StatusCode)
	}

	defer resp.Body.Close()
	var respData OAuthResponse

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// Read response data
	err = json.Unmarshal(body, &respData)
	if err != nil {
		return nil, err
	}

	return &respData, nil
}
