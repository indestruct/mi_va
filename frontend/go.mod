module bitbucket.org/indestruct/mi_va/frontend

go 1.14

require (
	github.com/jordan-wright/email v0.0.0-20200602115436-fd8a7622303e
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d
)
