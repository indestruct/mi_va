package main

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"time"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

const oauthGoogleURLAPI = "https://www.googleapis.com/oauth2/v2/userinfo?access_token="

// OauthGoogle implements handlers for logging via Google API
type OauthGoogle struct {
	config *oauth2.Config
}

// NewOauthGoogle creates new Google OAuth handler
func NewOauthGoogle() *OauthGoogle {
	return &OauthGoogle{
		config: &oauth2.Config{
			RedirectURL:  "http://mivatestapp.ddns.net:81/signin/google/callback",
			ClientID:     "241258462039-fc75schomb2ueabtb97n042trqh44v6o.apps.googleusercontent.com", //os.Getenv("GOOGLE_OAUTH_CLIENT_ID"),
			ClientSecret: "uUzAFaDcJSQjYdAXrM53kK3l",                                                 //os.Getenv("GOOGLE_OAUTH_CLIENT_SECRET"),
			Scopes:       []string{"https://www.googleapis.com/auth/userinfo.email"},
			Endpoint:     google.Endpoint,
		},
	}
}

// OauthGoogleLogin handles login via google
func (og *OauthGoogle) OauthGoogleLogin(w http.ResponseWriter, r *http.Request) {
	// Create state cookie
	oauthState := og.generateStateOauthCookie(w)
	u := og.config.AuthCodeURL(oauthState)

	// Redirect to google login
	http.Redirect(w, r, u, http.StatusTemporaryRedirect)
}

// OauthGoogleCallback Callback called from google API when user is authenticated
func (og *OauthGoogle) OauthGoogleCallback(w http.ResponseWriter, r *http.Request) {
	// Read state cookie
	oauthState, err := r.Cookie("oauthstate")

	if err != nil {
		log.Println("Could not fetch oauthstate from cookies")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	if r.FormValue("state") != oauthState.Value {
		log.Println("Invalid oauth google state")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	// Get the user token
	token, err := og.getToken(r.FormValue("code"))
	if err != nil {
		log.Println(err.Error())
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	// Authenticate user in backend
	bc := NewBackendClient()
	respData, err := bc.PostGoogleAuth(OAuthRequest{Token: token})

	if err != nil {
		log.Println("Error posting google auth to backend:", err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
	}

	// Set user cookies
	addCookie(w, "X-MIVA-User-Token", respData.Token, 60*time.Minute)
	addCookie(w, "X-MIVA-User-ID", strconv.FormatInt(respData.ID, 10), 60*time.Minute)

	// Redirect to edit page
	http.Redirect(w, r, "/profile", http.StatusPermanentRedirect)
}

func (og *OauthGoogle) generateStateOauthCookie(w http.ResponseWriter) string {
	var expiration = time.Now().Add(365 * 24 * time.Hour)

	b := make([]byte, 16)
	rand.Read(b)
	state := base64.URLEncoding.EncodeToString(b)
	cookie := http.Cookie{Name: "oauthstate", Value: state, Expires: expiration}
	http.SetCookie(w, &cookie)

	return state
}

func (og *OauthGoogle) getToken(code string) (string, error) {
	// Use code to get the user token
	token, err := og.config.Exchange(context.Background(), code)
	if err != nil {
		return "", fmt.Errorf("Could not exchange: %s", err.Error())
	}

	return token.AccessToken, nil
}
