package main

import (
	"log"
	"net/http"
	"net/smtp"
	"text/template"

	"github.com/jordan-wright/email"
)

const (
	emailAuth     = "PeMYK4xXQjv73KL"
	emailFrom     = "mivatestapp@gmail.com"
	emailSMTP     = "smtp.gmail.com"
	emailSMTPPort = "587"
)

type forgotPasswordData struct {
	PageTitle string
	Success   bool
}

// ForgotPasswordHandler handles request for resetting password
type ForgotPasswordHandler struct {
	forgot *template.Template
	reset  *template.Template
}

// NewForgotPasswordHandler creates new forgot password handler
func NewForgotPasswordHandler() *ForgotPasswordHandler {
	return &ForgotPasswordHandler{
		forgot: template.Must(template.ParseFiles("templates/forgot.html")),
		reset:  template.Must(template.ParseFiles("templates/reset.html")),
	}
}

// ForgotPasswordHandler handles request for sengind request for forgot password
func (fp *ForgotPasswordHandler) ForgotPasswordHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		bc := NewBackendClient()

		resp, err := bc.SendForgotPasswordRequest(ForgotPasswordRequest{Email: r.FormValue("email")})
		if err != nil {
			log.Println("Failed to send forgot password request", err)
			fp.forgot.Execute(w, &forgotPasswordData{
				PageTitle: "Reset your password",
				Success:   false,
			})
			return
		}

		resetURL := "http://mivatestapp.ddns.net:81/reset?token=" + resp.Token
		emailBody := "Follow the link to reset your password, it is valid 15 minutes:\n" + resetURL
		subject := "Reset your password"
		err = sendEmail(emailBody, r.FormValue("email"), subject)

		if err != nil {
			log.Println("Failed to send email", err)
		}

		fp.forgot.Execute(w, &forgotPasswordData{
			PageTitle: "Reset your password",
			Success:   err == nil,
		})
		return
	}
	fp.forgot.Execute(w, &forgotPasswordData{
		PageTitle: "Reset your password",
		Success:   false,
	})
}

// ResetPasswordHandler handles request for resetting password
func (fp *ForgotPasswordHandler) ResetPasswordHandler(w http.ResponseWriter, r *http.Request) {
	token := getURLParam(r, "token")

	if r.Method == http.MethodPost {
		bc := NewBackendClient()

		req := ResetPasswordRequest{
			Token:    token,
			Password: r.FormValue("password"),
		}
		err := bc.SendResetPasswordRequest(req)
		if err != nil {
			log.Println("Failed to send forgot password request", err)
		}
		http.Redirect(w, r, "/", http.StatusPermanentRedirect)
	}
	fp.reset.Execute(w, nil)
}

func getURLParam(r *http.Request, name string) string {
	keys, ok := r.URL.Query()[name]

	if !ok || len(keys[0]) < 1 {
		log.Printf("Url Param %s is missing", name)
		return ""
	}

	return keys[0]
}

func sendEmail(body, to, subject string) error {
	// Build the email
	e := email.NewEmail()
	e.From = emailFrom
	e.To = []string{to}
	e.Subject = subject
	e.Text = []byte(body)

	// Send the email
	err := e.Send(emailSMTP+":"+emailSMTPPort, smtp.PlainAuth("", emailFrom, emailAuth, emailSMTP))
	if err != nil {
		log.Println("Failed to send email", err)
	}

	return err
}
