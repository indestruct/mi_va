package main

import (
	"log"
	"net/http"
	"text/template"
)

// Profile defines the structure for profile template
type Profile struct {
	Edit      bool
	Email     string
	Fullname  string
	Address   string
	Telephone string
}

// ProfileHandler handles request for viewing profile data and editing
type ProfileHandler struct {
	template *template.Template
}

// NewProfileHandler creates new profile handler
func NewProfileHandler() *ProfileHandler {
	return &ProfileHandler{
		template: template.Must(template.ParseFiles("templates/profile.html")),
	}
}

// ShowProfile handles showing the profile page
func (ph *ProfileHandler) ShowProfile(w http.ResponseWriter, r *http.Request) {
	cookies, err := readCookies(r)
	if err != nil {
		http.Redirect(w, r, "/", http.StatusPermanentRedirect)
	}

	bc := NewBackendClient()
	user, err := bc.GetUser(cookies)
	if err != nil {
		log.Println("Failed getting user", err.Error())
		deleteCookie(w, "X-MIVA-User-Token")
		deleteCookie(w, "X-MIVA-User-ID")
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	ph.template.Execute(w, &Profile{
		Edit:      false,
		Email:     user.Email,
		Fullname:  user.Fullname,
		Address:   user.Address,
		Telephone: user.Telephone,
	})
}

// EditProfile handles editing the user profile
func (ph *ProfileHandler) EditProfile(w http.ResponseWriter, r *http.Request) {
	// Read cookies
	cookies, err := readCookies(r)
	if err != nil {
		log.Println("No cookies?")
		http.Redirect(w, r, "/", http.StatusPermanentRedirect)
	}

	bc := NewBackendClient()
	user, err := bc.GetUser(cookies)
	if err != nil {
		log.Println("Failed getting user", err.Error())
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	if r.Method == http.MethodPost {
		// Update profile
		user.Address = r.FormValue("address")
		user.Fullname = r.FormValue("fullname")
		user.Telephone = r.FormValue("telephone")

		err = bc.UpdateUser(cookies, user)
		if err != nil {
			log.Println("Failed to update user", err.Error())
			http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
			return
		}
		http.Redirect(w, r, "/profile", http.StatusPermanentRedirect)
	}

	ph.template.Execute(w, &Profile{
		Edit:      true,
		Email:     user.Email,
		Fullname:  user.Fullname,
		Address:   user.Address,
		Telephone: user.Telephone,
	})
}
