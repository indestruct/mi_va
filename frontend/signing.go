package main

import (
	"html/template"
	"log"
	"net/http"
	"strconv"
	"time"
)

// UserCookies contains data for available user cookies
type UserCookies struct {
	UserID string
	Token  string
}

// SigningHandler handles singing page routes
type SigningHandler struct {
	index  *template.Template
	signup *template.Template
}

// NewSigningHandler creates new signin handler
func NewSigningHandler() *SigningHandler {
	return &SigningHandler{
		index:  template.Must(template.ParseFiles("templates/index.html")),
		signup: template.Must(template.ParseFiles("templates/signup.html")),
	}
}

// SignoutHandler handles signing out user
func (sh *SigningHandler) SignoutHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost {
		// Read cookies
		cookies, err := readCookies(r)
		if err != nil {
			log.Println("Failed reading cookies", err)
		}

		bc := NewBackendClient()

		id, err := strconv.ParseInt(cookies.UserID, 10, 64)
		if err != nil {
			log.Println("Failed to read UserID from cookie", err)
		}

		req := OAuthResponse{ID: id, Token: cookies.Token}

		// Logout user
		err = bc.Logout(req)
		if err != nil {
			log.Println("Failed to logout user", err)
		}

		// Delete cookies
		deleteCookie(w, "X-MIVA-User-Token")
		deleteCookie(w, "X-MIVA-User-ID")
		clearSession(w)
		w.Header().Set("Cache-Control", "no-cache, no-store, must-revalidate")
		w.Header().Set("Pragma", "no-cache")
		w.Header().Set("Expires", "0")

		http.Redirect(w, r, "/", http.StatusPermanentRedirect)
	}
}

// SignupHandler handles signing up user
func (sh *SigningHandler) SignupHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		sh.signup.Execute(w, nil)
		return
	}

	bc := NewBackendClient()

	var u User
	u.Email = r.FormValue("email")
	u.Password = r.FormValue("password")

	// Create user in backend
	_, err := bc.CreateUser(&u)
	if err != nil {
		log.Println("Failed to create new user", err)
		return
	}

	req := AuthRequest{
		Email:    u.Email,
		Password: u.Password,
	}

	// Authorize user in backend
	resp, err := bc.PostAuth(req)
	if err != nil {
		log.Println("Failed to authenticate user", err)
		sh.signup.Execute(w, nil)
		return
	}

	// Set user cookies
	addCookie(w, "X-MIVA-User-Token", resp.Token, 60*time.Minute)
	addCookie(w, "X-MIVA-User-ID", strconv.FormatInt(resp.ID, 10), 60*time.Minute)

	// Redirect to edit page
	http.Redirect(w, r, "/edit", http.StatusPermanentRedirect)
}

// SigninHandler handles sign in of user
func (sh *SigningHandler) SigninHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		cookies, err := readCookies(r)
		// If user is already logged in, redirect to profile page
		if err == nil && cookies.Token != "" && cookies.UserID != "" {
			http.Redirect(w, r, "/profile", http.StatusPermanentRedirect)
		}

		sh.index.Execute(w, nil)
		return
	}

	req := AuthRequest{
		Email:    r.FormValue("email"),
		Password: r.FormValue("password"),
	}

	bc := NewBackendClient()

	// Authorize user in backend
	resp, err := bc.PostAuth(req)
	if err != nil {
		log.Println("Failed to authenticate user", err)
		sh.index.Execute(w, nil)
		return
	}

	// Set user cookies
	addCookie(w, "X-MIVA-User-Token", resp.Token, 60*time.Minute)
	addCookie(w, "X-MIVA-User-ID", strconv.FormatInt(resp.ID, 10), 60*time.Minute)

	http.Redirect(w, r, "/profile", http.StatusPermanentRedirect)
}

func readCookies(r *http.Request) (UserCookies, error) {
	var cookies UserCookies
	cookie, err := r.Cookie("X-MIVA-User-ID")
	if err != nil {
		return cookies, err
	}
	cookies.UserID = cookie.Value

	cookie, err = r.Cookie("X-MIVA-User-Token")
	if err != nil {
		return cookies, err
	}
	cookies.Token = cookie.Value

	return cookies, nil
}

func addCookie(w http.ResponseWriter, name, value string, ttl time.Duration) {
	expire := time.Now().Add(ttl)
	cookie := http.Cookie{
		Name:     name,
		Value:    value,
		Expires:  expire,
		Path:     "/",
		HttpOnly: true,
	}
	http.SetCookie(w, &cookie)
}

func deleteCookie(w http.ResponseWriter, name string) {
	c := &http.Cookie{
		Name:     name,
		Value:    "",
		Expires:  time.Unix(0, 0),
		Path:     "/",
		HttpOnly: true,
	}

	http.SetCookie(w, c)
}

func clearSession(response http.ResponseWriter) {
	cookie := &http.Cookie{
		Name:   "session",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}
	http.SetCookie(response, cookie)
}
