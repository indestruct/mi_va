package main

import (
	"log"
	"net/http"
)

func main() {
	oauth := NewOauthGoogle()
	ph := NewProfileHandler()
	sh := NewSigningHandler()
	fp := NewForgotPasswordHandler()

	// Google signing handlers
	http.HandleFunc("/signin/google", oauth.OauthGoogleLogin)
	http.HandleFunc("/signin/google/callback", oauth.OauthGoogleCallback)

	// Profile handlers
	http.HandleFunc("/profile", ph.ShowProfile)
	http.HandleFunc("/edit", ph.EditProfile)

	// Signing handlers
	http.HandleFunc("/signup", sh.SignupHandler)
	http.HandleFunc("/signout", sh.SignoutHandler)
	http.HandleFunc("/", sh.SigninHandler)

	// ForgotPassword handlers
	http.HandleFunc("/forgot", fp.ForgotPasswordHandler)
	http.HandleFunc("/reset", fp.ResetPasswordHandler)

	err := http.ListenAndServe(":8081", nil)

	if err != nil {
		log.Fatal(err)
	}
}
