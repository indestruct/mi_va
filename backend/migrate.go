package main

import (
	"database/sql"
	"log"
)

const (
	// Version Current version of database migrate
	Version = 1
	// Schema Current schema of the database
	Schema = "mysql"
	// VersionTableName The table where version is stored
	VersionTableName = "version"

	createUsersQuery = `
    CREATE TABLE users (
        id BIGINT AUTO_INCREMENT,
        email VARCHAR(256) NOT NULL UNIQUE,
        password TEXT NOT NULL,
		created_at BIGINT NOT NULL,
		updated_at BIGINT NOT NULL,
		fullname VARCHAR(256),
		address  VARCHAR(512),
		telephone VARCHAR(128),
		version INT,
        PRIMARY KEY (id)
	);`
	createTokenQuery = `
	CREATE TABLE tokens (
		id INT AUTO_INCREMENT,
		userid BIGINT NOT NULL,
		token VARCHAR(256) NOT NULL,
        PRIMARY KEY (id)
	);`
	createVersionQuery = `
	CREATE TABLE version (
        id INT NOT NULL,
        PRIMARY KEY (id)
	);`
	versionExistQuery = `
	SELECT IF(COUNT(*),'true','false') 
	FROM information_schema.tables
	WHERE table_schema = ? 
		AND table_name = ?
	LIMIT 1;`
	insertVersionQuery = `
	INSERT INTO version (id) VALUES (?)
	`
	getVersionQuery = `
	SELECT id FROM version
	ORDER BY id DESC
	LIMIT 1;`
)

// DbMigrate migrates the database to current version. If DB is new, it is initialized.
func DbMigrate(db *sql.DB) error {
	var doesExist bool

	var debug = true
	if debug {
		db.Exec("DROP TABLE users;")
		db.Exec("DROP TABLE version;")
		db.Exec("DROP TABLE tokens;")
	}

	err := db.QueryRow(versionExistQuery, Schema, VersionTableName).Scan(&doesExist)
	if err != nil {
		log.Fatal(err)
		return err
	}

	if !doesExist {
		if err = initialize(db); err != nil {
			log.Fatal(err)
			return err
		}

	}

	if err = migrate(db); err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}

func initialize(db *sql.DB) error {
	// Create the version table.
	_, err := db.Exec(createVersionQuery)
	if err != nil {
		log.Fatal(err)
		return err
	}

	// Set the current version
	_, err = db.Exec(insertVersionQuery, Version)
	if err != nil {
		log.Fatal(err)
		return err
	}

	// Create the user table.
	_, err = db.Exec(createUsersQuery)
	if err != nil {
		log.Fatal(err)
		return err
	}

	// Create tokens table
	_, err = db.Exec(createTokenQuery)
	if err != nil {
		log.Fatal(err)
		return err
	}

	return nil
}

func migrate(db *sql.DB) error {
	var dbVersion int

	if err := db.QueryRow(getVersionQuery).Scan(&dbVersion); err != nil {
		log.Fatal(err)
		return err
	}

	if dbVersion < Version {
		// TODO: migrate
	}

	return nil
}
