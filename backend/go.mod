module bitbucket.org/indestruct/mi_va/backend

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/gorilla/mux v1.7.4
	golang.org/x/crypto v0.0.0-20200604202706-70a84ac30bf9
	golang.org/x/oauth2 v0.0.0-20200107190931-bf48bf16ab8d // indirect
)
