package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"

	"bitbucket.org/indestruct/mi_va/backend/tokens"
	"bitbucket.org/indestruct/mi_va/backend/users"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

// WebServer serves backend requests
type WebServer struct {
	db           *sql.DB
	router       *mux.Router
	usersModel   *users.Model
	tokenHandler *tokens.JWTTokenHandler
}

type authRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

type oAuthRequest struct {
	Token string `json:"token"`
}

type oAuthResponse struct {
	ID    int64  `json:"id"`
	Token string `json:"token"`
}

type forgotPasswordRequest struct {
	Email string `json:"email"`
}

type forgotPasswordResponse struct {
	Token string `json:"token"`
}

type resetPassword struct {
	Password string `json:"password"`
	Token    string `json:"token"`
}

// NewWebServer creates new backend web server
func NewWebServer() (*WebServer, error) {
	// Configure the database connection

	db, err := sql.Open("mysql", "root:root@(db:3306)/mysql?parseTime=true")

	// Migrate database
	if err = DbMigrate(db); err != nil {
		return nil, err
	}

	userStorage := users.NewUserStorage(db)
	tokenStorage := tokens.NewTokenStorage(db)

	return &WebServer{
		db:           db,
		router:       mux.NewRouter(),
		usersModel:   users.NewUserModel(userStorage),
		tokenHandler: tokens.NewJWTTokenHandler(tokenStorage),
	}, nil
}

// Run runs the backend web server
func (ws *WebServer) Run() {
	ws.initEndpoints()

	if err := http.ListenAndServe(":8000", ws.router); err != nil {
		log.Fatal(err)
	}
}

// CheckAuthentication Middleware for verifying authorization of API calls
func (ws *WebServer) CheckAuthentication(f http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// get token string from headers
		tokenString := r.Header.Get("token")

		// Validate token
		_, err := ws.tokenHandler.ValidateToken(tokenString)

		if err != nil {
			fmt.Println(err)
			http.Error(w, fmt.Errorf("Not Authorized").Error(), http.StatusMethodNotAllowed)
			return
		}

		f(w, r)
	}
}

// OnUserAuth http handler for standard authentication of users
func (ws *WebServer) OnUserAuth(w http.ResponseWriter, r *http.Request) {
	var userAuth authRequest

	if err := json.NewDecoder(r.Body).Decode(&userAuth); err != nil {
		log.Println("Failed decoding auth")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err := ws.usersModel.GetUserByEmail(userAuth.Email)
	if err != nil {
		log.Println("Failed getting user by email", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if user == nil {
		log.Println("User does not exist")
		http.Error(w, fmt.Errorf("User does not exist").Error(), http.StatusBadRequest)
		return
	}

	// Check user password
	passwordMatch, err := ws.usersModel.CheckPasswordHash(userAuth.Password, ws.usersModel.GetUserPassword(user.ID))

	if !passwordMatch {
		log.Println("Password did not match", err)
		http.Error(w, fmt.Errorf("Password did not match").Error(), http.StatusBadRequest)
		return
	}

	token, err := ws.tokenHandler.NewToken(user.ID, time.Hour*24)

	if err != nil {
		log.Println("Error issuing token", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := oAuthResponse{
		Token: token.Token,
		ID:    token.UserID,
	}

	w.Header().Set("Content-Type", "application/json")

	if err = json.NewEncoder(w).Encode(resp); err != nil {
		log.Fatal("Faled to encode user", err)
	}
}

// OnUserGoogleAuth http handler for google authentication of users
func (ws *WebServer) OnUserGoogleAuth(w http.ResponseWriter, r *http.Request) {
	var userAuth oAuthRequest

	if err := json.NewDecoder(r.Body).Decode(&userAuth); err != nil {
		log.Println("Failed decoding auth")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	userInfo, err := users.GetGoogleUserData(userAuth.Token)
	if err != nil {
		log.Println("Failed getting user data")
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err := ws.usersModel.GetUserByEmail(userInfo.Email)
	if err != nil {
		log.Println("Failed getting user by email", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if user == nil {
		user, err = ws.usersModel.CreateNewUserFromEmail(userInfo.Email)
		if err != nil {
			log.Println("Failed creating user by email", err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	token, err := ws.tokenHandler.NewToken(user.ID, time.Hour*24)

	if err != nil {
		log.Println("Error issuing token", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := oAuthResponse{
		Token: token.Token,
		ID:    token.UserID,
	}

	w.Header().Set("Content-Type", "application/json")

	if err = json.NewEncoder(w).Encode(resp); err != nil {
		log.Fatal("Faled to encode user", err)
	}
}

// OnUserLogout handles logging out of user
func (ws *WebServer) OnUserLogout(w http.ResponseWriter, r *http.Request) {
	var userAuth oAuthResponse

	if err := json.NewDecoder(r.Body).Decode(&userAuth); err != nil {
		log.Println("Failed decoding auth", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Invalidate user token
	err := ws.tokenHandler.InvalidateToken(userAuth.Token)

	if err != nil {
		log.Println("Failed to invalidate token", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// OnForgotPassword generates token for user to reset password
func (ws *WebServer) OnForgotPassword(w http.ResponseWriter, r *http.Request) {
	var req forgotPasswordRequest

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println("Failed decoding auth", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err := ws.usersModel.GetUserByEmail(req.Email)
	if err != nil {
		log.Println("Failed getting user by email", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if user == nil {
		log.Println("User does not exist")
		http.Error(w, fmt.Errorf("User does not exist").Error(), http.StatusBadRequest)
		return
	}

	// Create token for reset password
	token, err := ws.tokenHandler.NewToken(user.ID, time.Minute*15)

	if err != nil {
		log.Println("Error issuing token", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := forgotPasswordResponse{
		Token: token.Token,
	}

	w.Header().Set("Content-Type", "application/json")

	if err = json.NewEncoder(w).Encode(resp); err != nil {
		log.Fatal("Faled to encode user", err)
	}
}

// OnResetPassword resets user password to new
func (ws *WebServer) OnResetPassword(w http.ResponseWriter, r *http.Request) {
	var req resetPassword

	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		log.Println("Failed decoding auth", err)
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	// Validate token
	id, err := ws.tokenHandler.ValidateToken(req.Token)
	if err != nil {
		fmt.Println(err)
		http.Error(w, fmt.Errorf("Not Authorized").Error(), http.StatusBadRequest)
		return
	}

	// Invalidate token
	err = ws.tokenHandler.InvalidateToken(req.Token)
	if err != nil {
		log.Println("Failed invalidating reset password token", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Reset user password
	err = ws.usersModel.SetUserPassword(id, req.Password)
	if err != nil {
		log.Println("Failed setting user password", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (ws *WebServer) initEndpoints() {
	ws.router.HandleFunc("/api/users", ws.usersModel.OnCreateUser).Methods("POST")
	ws.router.HandleFunc("/api/users/{id:[0-9]+}", ws.CheckAuthentication(ws.usersModel.OnUpdateUser)).Methods("PUT")
	ws.router.HandleFunc("/api/users/{id:[0-9]+}", ws.CheckAuthentication(ws.usersModel.OnDeleteUser)).Methods("DELETE")
	ws.router.HandleFunc("/api/users/{id:[0-9]+}", ws.CheckAuthentication(ws.usersModel.OnGetUser)).Methods("GET")
	ws.router.HandleFunc("/api/users/authenticate", ws.OnUserAuth).Methods("POST")
	ws.router.HandleFunc("/api/users/authenticate/google", ws.OnUserGoogleAuth).Methods("POST")
	ws.router.HandleFunc("/api/users/logout", ws.CheckAuthentication(ws.OnUserLogout)).Methods("POST")
	ws.router.HandleFunc("/api/users/forgot", ws.OnForgotPassword).Methods("POST")
	ws.router.HandleFunc("/api/users/reset", ws.OnResetPassword).Methods("POST")
}
