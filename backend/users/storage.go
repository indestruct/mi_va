package users

import (
	"database/sql"
)

const (
	insertUserQuery     = `INSERT INTO users (email, password, fullname, address, telephone, created_at, updated_at, version) VALUES (?, ?, ?, ?, ?, ?, ?, ?);`
	getUserQuery        = `SELECT id, email, fullname, address, telephone, created_at, updated_at, version FROM users WHERE id = ?;`
	getUserByEmailQuery = `SELECT id, email, fullname, address, telephone, created_at, updated_at, version FROM users WHERE email = ?;`
	updateUserQuery     = `UPDATE users SET email=?, fullname=?, address=?, telephone=?, updated_at=?, version=? WHERE id = ?;`
	deleteUserQuery     = `DELETE FROM users WHERE id = ?`
	getPasswordQuery    = `SELECT password FROM users WHERE id = ?`
	setPasswordQuery    = `UPDATE users SET password = ? WHERE id = ?`
)

// StorageImpl implements Users Storage interface
type StorageImpl struct {
	db *sql.DB
}

// NewUserStorage creates new instance of storage implementation
func NewUserStorage(db *sql.DB) *StorageImpl {
	return &StorageImpl{
		db: db,
	}
}

// CreateUser inserts new user into database, returns the generated ID for that user
func (usi *StorageImpl) CreateUser(u *User) (int64, error) {
	r, err := usi.db.Exec(insertUserQuery, u.Email, u.Password, u.Fullname, u.Address, u.Telephone, u.CreatedAt, u.UpdatedAt, u.Version)

	if err != nil {
		return 0, err
	}

	id, err := r.LastInsertId()

	if err != nil {
		return 0, err
	}

	return id, nil
}

// GetUser queries database for existing user, returns nil if not found
func (usi *StorageImpl) GetUser(id int64) (*User, error) {
	var u User
	row := usi.db.QueryRow(getUserQuery, id)
	err := row.Scan(&u.ID, &u.Email, &u.Fullname, &u.Address, &u.Telephone, &u.CreatedAt, &u.UpdatedAt, &u.Version)

	if err != nil && err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}

	return &u, nil
}

// GetUserByEmail queries database for existing user by email, returns nil if not found
func (usi *StorageImpl) GetUserByEmail(email string) (*User, error) {
	var u User
	row := usi.db.QueryRow(getUserByEmailQuery, email)
	err := row.Scan(&u.ID, &u.Email, &u.Fullname, &u.Address, &u.Telephone, &u.CreatedAt, &u.UpdatedAt, &u.Version)

	if err != nil && err == sql.ErrNoRows {
		return nil, nil
	}

	if err != nil {
		return nil, err
	}

	return &u, nil
}

// GetUserPassword queries password for user
func (usi *StorageImpl) GetUserPassword(userID int64) (string, error) {
	var password string
	row := usi.db.QueryRow(getPasswordQuery, userID)
	err := row.Scan(&password)

	if err != nil && err == sql.ErrNoRows {
		return "", nil
	}

	if err != nil {
		return "", err
	}

	return password, nil

}

// SetUserPassword updates password for user
func (usi *StorageImpl) SetUserPassword(userID int64, password string) error {
	_, err := usi.db.Exec(setPasswordQuery, password, userID)
	return err
}

// UpdateUser updates properties of existing user in database
func (usi *StorageImpl) UpdateUser(u *User) error {
	_, err := usi.db.Exec(updateUserQuery, u.Email, u.Fullname, u.Address, u.Telephone, u.UpdatedAt, u.Version, u.ID)
	return err
}

// DeleteUser detelets existing user from database
func (usi *StorageImpl) DeleteUser(id int64) error {
	_, err := usi.db.Exec(deleteUserQuery, id)
	return err
}
