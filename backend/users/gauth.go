package users

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// GoogleUserInfo defines the information for user from google
type GoogleUserInfo struct {
	ID            string `json:"id"`
	Email         string `json:"email"`
	VerifiedEmail bool   `json:"verified_email,omitempty"`
	Picture       string `json:"picture,omitempty"`
}

const oauthGoogleURLAPI = "https://www.googleapis.com/oauth2/v2/userinfo?access_token="

// GetGoogleUserData fetch user data from google by given token
func GetGoogleUserData(token string) (GoogleUserInfo, error) {
	var userInfo GoogleUserInfo

	// Verify the token is valid and get the user data
	response, err := http.Get(oauthGoogleURLAPI + token)
	if err != nil {
		return userInfo, fmt.Errorf("Failed getting user info: %s", err.Error())
	}

	defer response.Body.Close()
	data, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return userInfo, fmt.Errorf("Failed read response: %s", err.Error())
	}

	err = json.Unmarshal(data, &userInfo)
	if err != nil {
		return userInfo, fmt.Errorf("Failed unmarshalling response: %s", err.Error())
	}

	return userInfo, nil
}
