package users

import (
	"encoding/json"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"golang.org/x/crypto/bcrypt"
)

// User defines the structure to be used in users REST API
type User struct {
	ID        int64  `json:"id"`
	Email     string `json:"email"`
	Password  string `json:"password,omitempty"`
	Fullname  string `json:"fullname"`
	Address   string `json:"address"`
	Telephone string `json:"telephone"`
	CreatedAt int64  `json:"createdAt"`
	UpdatedAt int64  `json:"updatedAt"`
	Version   int    `json:"version"`
}

// UserAuthenticate defines the structure to be used to authenticate users
type UserAuthenticate struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

// Storage defines interface to the database storage
type Storage interface {
	CreateUser(u *User) (int64, error)
	GetUser(id int64) (*User, error)
	GetUserByEmail(email string) (*User, error)
	GetUserPassword(userID int64) (string, error)
	SetUserPassword(userID int64, password string) error
	UpdateUser(u *User) error
	DeleteUser(id int64) error
}

// Model defines the Users model
type Model struct {
	storage Storage
}

// NewUserModel creates new User Model instance
func NewUserModel(st Storage) *Model {
	return &Model{
		storage: st,
	}
}

// OnCreateUser http handler for creating new user
func (um *Model) OnCreateUser(w http.ResponseWriter, r *http.Request) {
	var user User

	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user.CreatedAt = time.Now().Unix()
	user.Version = 0

	// Calculate password hash
	hash, err := um.HashPassword(user.Password)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Set the hash of the password
	user.Password = hash

	id, err := um.storage.CreateUser(&user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	user.ID = id
	user.Password = ""

	w.Header().Set("Content-Type", "application/json")

	if err := json.NewEncoder(w).Encode(user); err != nil {
		log.Fatal("Faled to encode user", err)
	}
}

// OnUpdateUser http handler for updating existing user
func (um *Model) OnUpdateUser(w http.ResponseWriter, r *http.Request) {
	var user User
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	existing, err := um.storage.GetUser(id)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if existing == nil {
		http.Error(w, "User not found", http.StatusNotFound)
		return
	}

	if existing.Version != user.Version {
		http.Error(w, "User version is outdated", http.StatusBadRequest)
		return
	}

	user.Version++
	user.ID = id

	if err = um.storage.UpdateUser(&user); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json")

	if err = json.NewEncoder(w).Encode(user); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

// OnDeleteUser http handler for deleting users
func (um *Model) OnDeleteUser(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	if err = um.storage.DeleteUser(id); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// OnGetUser http handler for getting information for existing user
func (um *Model) OnGetUser(w http.ResponseWriter, r *http.Request) {
	id, err := strconv.ParseInt(mux.Vars(r)["id"], 10, 64)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	user, err := um.storage.GetUser(id)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	if user == nil {
		http.Error(w, "User not found", http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json")

	if err = json.NewEncoder(w).Encode(user); err != nil {
		log.Fatal("Faled to encode user", err)
	}
}

// GetUserByEmail gets user by email
func (um *Model) GetUserByEmail(email string) (*User, error) {
	return um.storage.GetUserByEmail(email)
}

// CreateNewUserFromEmail creates new user by email
func (um *Model) CreateNewUserFromEmail(email string) (*User, error) {
	u := &User{
		Email:     email,
		CreatedAt: time.Now().Unix(),
		Version:   0,
	}

	id, err := um.storage.CreateUser(u)
	if err != nil {
		return nil, err
	}

	return um.storage.GetUser(id)
}

// OnUserAuthenticate http handler for authentication of users
func (um *Model) OnUserAuthenticate(w http.ResponseWriter, r *http.Request) {
	var userAuth UserAuthenticate

	if err := json.NewDecoder(r.Body).Decode(&userAuth); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

}

// GetUserPassword gets password for user
func (um *Model) GetUserPassword(id int64) string {
	password, err := um.storage.GetUserPassword(id)
	if err != nil {
		log.Println("Failed getting user password", err)
	}
	return password
}

// SetUserPassword updates password for user
func (um *Model) SetUserPassword(userID int64, password string) error {
	hashed, err := um.HashPassword(password)
	if err != nil {
		return err
	}

	return um.storage.SetUserPassword(userID, hashed)
}

// HashPassword encrypts user password
func (um *Model) HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

// CheckPasswordHash verifies if passowrd is the same as the encrypted hash
func (um *Model) CheckPasswordHash(password, hash string) (bool, error) {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil, err
}
