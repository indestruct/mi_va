package tokens

import (
	"fmt"
	"strconv"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
)

// Used to encrypt/decrypt JWT tokens
const jwtTokenSecret = "58NSWpr3oerBP2QLQfupAN6dUBnt3XOgt7CbMaq2HOwEAMwNFp7JckC9Em1IDSU"

// JWTToken cointains the data that is stored for token
type JWTToken struct {
	UserID int64
	Token  string
}
type JWTStorage interface {
	StoreToken(token *JWTToken) error
	IsTokenValid(token *JWTToken) (bool, error)
	InvalidateToken(token *JWTToken) error
}
type JWTTokenHandler struct {
	storage JWTStorage
}

// NewJWTTokenHandler creates new JWT token handler
func NewJWTTokenHandler(st JWTStorage) *JWTTokenHandler {
	return &JWTTokenHandler{
		storage: st,
	}
}

// NewToken issues new token for user
func (th *JWTTokenHandler) NewToken(userID int64, validFor time.Duration) (*JWTToken, error) {
	claims := jwt.MapClaims{}
	claims["authorized"] = true
	claims["userid"] = strconv.FormatInt(userID, 10)
	claims["expired"] = time.Now().Add(validFor).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenStr, err := token.SignedString([]byte(jwtTokenSecret))
	if err != nil {
		return nil, err
	}

	jwtToken := &JWTToken{
		UserID: userID,
		Token:  tokenStr,
	}

	err = th.storage.StoreToken(jwtToken)
	if err != nil {
		return nil, err
	}

	return jwtToken, nil
}

func (th *JWTTokenHandler) ValidateToken(token string) (int64, error) {
	userID, err := th.parseToken(token)
	if err != nil {
		return 0, err
	}

	tkn := &JWTToken{
		UserID: userID,
		Token:  token,
	}

	isValid, err := th.storage.IsTokenValid(tkn)

	if err != nil || !isValid {
		return 0, fmt.Errorf("Token in invalid")
	}

	return userID, nil
}

func (th *JWTTokenHandler) parseToken(token string) (int64, error) {
	if token == "" {
		return 0, fmt.Errorf("Token string is empty")
	}

	// Parse the token string
	jwtToken, err := jwt.Parse(token, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}
		return []byte(jwtTokenSecret), nil
	})

	if err != nil {
		return 0, err
	}

	claims, ok := jwtToken.Claims.(jwt.MapClaims)
	if !ok || !jwtToken.Valid {
		return 0, fmt.Errorf("Token is invalid")

	}

	userID, ok := claims["userid"].(string)
	if !ok {
		return 0, fmt.Errorf("Failed to get userID from token")
	}

	id, err := strconv.ParseInt(userID, 10, 64)
	if !ok {
		return 0, fmt.Errorf("Failed to convert userID from token")
	}

	return id, nil
}

func (th *JWTTokenHandler) InvalidateToken(token string) error {
	userID, err := th.parseToken(token)

	fmt.Println("Invalidate token caled")

	if err != nil {
		return err
	}

	tkn := &JWTToken{
		UserID: userID,
		Token:  token,
	}

	if err := th.storage.InvalidateToken(tkn); err != nil {
		return err
	}
	return nil
}
