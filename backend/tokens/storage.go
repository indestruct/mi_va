package tokens

import "database/sql"

const (
	insertTokenQuery = `INSERT INTO tokens (userid, token) VALUES (?, ?);`
	quertToken       = `SELECT IF(COUNT(*),'true','false') FROM tokens WHERE userid = ? AND token = ?;`
	deleteToken      = `DELETE FROM tokens WHERE userid = ? AND token = ?`
)

type JWTStorageImpl struct {
	db *sql.DB
}

func NewTokenStorage(db *sql.DB) *JWTStorageImpl {
	return &JWTStorageImpl{
		db: db,
	}
}

func (si *JWTStorageImpl) StoreToken(token *JWTToken) error {
	_, err := si.db.Exec(insertTokenQuery, token.UserID, token.Token)

	if err != nil {
		return err
	}

	return nil
}

func (si *JWTStorageImpl) IsTokenValid(token *JWTToken) (bool, error) {
	var doesExist bool
	err := si.db.QueryRow(quertToken, token.UserID, token.Token).Scan(&doesExist)
	if err != nil {
		return false, err
	}
	return doesExist, nil
}

func (si *JWTStorageImpl) InvalidateToken(token *JWTToken) error {
	_, err := si.db.Exec(deleteToken, token.UserID, token.Token)

	if err != nil {
		return err
	}

	return nil
}
