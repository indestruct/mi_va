package main

import "log"

func db() {

}

func main() {
	ws, err := NewWebServer()

	if err != nil {
		log.Fatal(err)
		return
	}

	ws.Run()
}
